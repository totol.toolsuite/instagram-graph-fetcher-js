# instagram-graph-fetcher-js

## Package in order to simplify access to instagram graph api.
- Refresh your access token
- Fetch user datas
- Fetch user medias datas

## Requirement 
- Instagram user access token : [Create an instagram app and register your account, then get your token](https://developers.facebook.com/docs/instagram-basic-display-api)

## Usage

### Installation
``` npm i instagram-graph-fetcher-js ```

#### Example
``` 
import { fetchMediaFields } from "instagram-graph-fetcher-js"; 
fetchFields(
  "<access_token>",
  ["media_url", "permalink", "media_type"]
).then((x) => console.log(x));
```

## Methods list
#### refreshToken
##### Description
- Pass it your token, and if it has at least 24h, it will refresh it so it has still 60 days last
##### Usage
``` 
import { refreshToken } from "instagram-graph-fetcher-js"; 
refreshToken(
  "<access_token>"
).then((x) => console.log(x));
```
##### Parameter
- access_token : The user access token
##### Response
```
{
   "access_token": "<new_token>",
   "token_type": "bearer",
   "expires_in": <time left in seconds>
}
```

#### fetchMediaFields
##### Description
- Pass it your token, and the field you wants, will return you a promise with the fields infos
##### Usage
``` 
import { fetchMediaFields } from "instagram-graph-fetcher-js"; 
fetchMediaFields(
  <access_token>,["media_url", "permalink", "media_type"])
  .then((response) => console.log(response));
```
##### Parameter
- access_token : The user access token
- fields : [The list of fields you want](https://developers.facebook.com/docs/instagram-basic-display-api/reference/media)
##### Response
```
{
    "data": [
        {
            "media_url": "<url>",
            "permalink": "<link>",
            "media_type": "IMAGE",
            "id": "<id>"
        },
        {
            "media_url": "<url>",
            "permalink": "<link>",
            "media_type": "IMAGE",
            "id": "<id>"
        },
        {
          ...
        }
    ],
    "paging": {
        "cursors": {
            "before": "<token>",
            "after": "<token>"
        }
    }
}
```

#### fetchUserFields
##### Description
- Pass it your token, and the field you wants, will return you a promise with the fields infos
##### Usage
``` 
import { fetchUserFields } from "instagram-graph-fetcher-js"; 
fetchUserFields(
  <access_token>,["id","username"])
  .then((response) => console.log(response));
```
##### Parameter
- access_token : The user access token
- fields : [The list of fields you want](https://developers.facebook.com/docs/instagram-basic-display-api/reference/user)
##### Response
```
{
    "id": "<id>",
    "username": "<username>"
}
```

#### fetchSpecificMediaFields
##### Description
- Pass it your token, and the field you wants, will return you a promise with the fields infos
##### Usage
- When you have the media id list with fetchUserFields("access_token", ["media"]), you can request infos of this specific media
``` 
import { fetchSpecificMediaFields } from "instagram-graph-fetcher-js"; 
fetchSpecificMediaFields(
  <access_token>, <media_url>,["permalink", "media_type"])
  .then((response) => console.log(response));
```
##### Parameter
- access_token : The user access token
- fields : [The list of fields you want](https://developers.facebook.com/docs/instagram-basic-display-api/reference/media)
##### Response
```
{
    "media_url": "<media_url>",
    "id": "<media_url>"
}
```



