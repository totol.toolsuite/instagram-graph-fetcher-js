import axios from "axios";
import {
  INSTAGRAM_FETCH_MEDIA_URL,
  INSTAGRAM_FETCH_USER_URL,
  INSTAGRAM_API_PREFIX,
} from "./constants";

/**
 * @returns promise with the asked fields
 *
 * @example fetchUserFields(<access_token>,["media", "media_count", "username"]).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 * @param {User fields, fields list at "https://developers.facebook.com/docs/instagram-basic-display-api/reference/user"} fields
 */
export const fetchUserFields = (accessToken, fields) => {
  const params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("fields", fields.join(","));

  return axios
    .get(INSTAGRAM_FETCH_USER_URL, { params: params })
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      return err;
    });
};

/**
 * @returns promise with the asked fields
 *
 * @example fetchMediaFields(<access_token>,["media_url", "permalink", "media_type"]).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 * @param {Media fields, fields list at "https://developers.facebook.com/docs/instagram-basic-display-api/reference/media"} fields
 */
export const fetchMediaFields = (accessToken, fields) => {
  const params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("fields", fields.join(","));
  return axios
    .get(INSTAGRAM_FETCH_MEDIA_URL, { params: params })
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      return err;
    });
};

/**
 * @returns promise with the asked fields of the asked media
 *
 * @example fetchSpecificMediaFields(<access_token>, <media_url>,["permalink", "media_type"]).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 * @param {Media fields, fields list at "https://developers.facebook.com/docs/instagram-basic-display-api/reference/media"} fields
 */
export const fetchSpecificMediaFields = (accessToken, media_url, fields) => {
  const params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("fields", fields.join(","));
  return axios
    .get(`${INSTAGRAM_API_PREFIX}${media_url}`, { params: params })
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      return err;
    });
};
