export const INSTAGRAM_API_PREFIX = "https://graph.instagram.com/";
export const INSTAGRAM_FETCH_MEDIA_URL = "https://graph.instagram.com/me/media";
export const INSTAGRAM_FETCH_USER_URL = "https://graph.instagram.com/me";
export const INSTAGRAM_REFRESH_TOKEN =
  "https://graph.instagram.com/refresh_access_token";
