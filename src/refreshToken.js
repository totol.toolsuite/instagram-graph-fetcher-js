import axios from "axios";
import { INSTAGRAM_REFRESH_TOKEN } from "./constants";

/**
 * @returns promise with the new token and its duration
 *
 * @example refreshToken(<access_token>).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 */
export const refreshToken = (accessToken) => {
  const params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("grant_type", "ig_refresh_token");

  return axios
    .get(INSTAGRAM_REFRESH_TOKEN, { params: params })
    .then((response) => {
      return response.data;
    })
    .catch((err) => {
      return err;
    });
};
