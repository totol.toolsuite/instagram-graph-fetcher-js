"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.refreshToken = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @returns promise with the new token and its duration
 *
 * @example refreshToken(<access_token>).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 */
var refreshToken = function refreshToken(accessToken) {
  var params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("grant_type", "ig_refresh_token");
  return _axios["default"].get(_constants.INSTAGRAM_REFRESH_TOKEN, {
    params: params
  }).then(function (response) {
    return response.data;
  })["catch"](function (err) {
    return err;
  });
};

exports.refreshToken = refreshToken;