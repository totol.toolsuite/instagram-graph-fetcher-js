"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchSpecificMediaFields = exports.fetchMediaFields = exports.fetchUserFields = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * @returns promise with the asked fields
 *
 * @example fetchUserFields(<access_token>,["media", "media_count", "username"]).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 * @param {User fields, fields list at "https://developers.facebook.com/docs/instagram-basic-display-api/reference/user"} fields
 */
var fetchUserFields = function fetchUserFields(accessToken, fields) {
  var params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("fields", fields.join(","));
  return _axios["default"].get(_constants.INSTAGRAM_FETCH_USER_URL, {
    params: params
  }).then(function (response) {
    return response.data;
  })["catch"](function (err) {
    return err;
  });
};
/**
 * @returns promise with the asked fields
 *
 * @example fetchMediaFields(<access_token>,["media_url", "permalink", "media_type"]).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 * @param {Media fields, fields list at "https://developers.facebook.com/docs/instagram-basic-display-api/reference/media"} fields
 */


exports.fetchUserFields = fetchUserFields;

var fetchMediaFields = function fetchMediaFields(accessToken, fields) {
  var params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("fields", fields.join(","));
  return _axios["default"].get(_constants.INSTAGRAM_FETCH_MEDIA_URL, {
    params: params
  }).then(function (response) {
    return response.data;
  })["catch"](function (err) {
    return err;
  });
};
/**
 * @returns promise with the asked fields of the asked media
 *
 * @example fetchSpecificMediaFields(<access_token>, <media_url>,["permalink", "media_type"]).then((response) => console.log(response));
 *
 * @param {The access token of the user, retrieve it at "https://developers.facebook.com/docs/instagram-basic-display-api/overview#instagram-user-access-tokens"} accessToken
 * @param {Media fields, fields list at "https://developers.facebook.com/docs/instagram-basic-display-api/reference/media"} fields
 */


exports.fetchMediaFields = fetchMediaFields;

var fetchSpecificMediaFields = function fetchSpecificMediaFields(accessToken, media_url, fields) {
  var params = new URLSearchParams();
  params.append("access_token", accessToken);
  params.append("fields", fields.join(","));
  return _axios["default"].get("".concat(_constants.INSTAGRAM_API_PREFIX).concat(media_url), {
    params: params
  }).then(function (response) {
    return response.data;
  })["catch"](function (err) {
    return err;
  });
};

exports.fetchSpecificMediaFields = fetchSpecificMediaFields;