"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.INSTAGRAM_REFRESH_TOKEN = exports.INSTAGRAM_FETCH_USER_URL = exports.INSTAGRAM_FETCH_MEDIA_URL = exports.INSTAGRAM_API_PREFIX = void 0;
var INSTAGRAM_API_PREFIX = "https://graph.instagram.com/";
exports.INSTAGRAM_API_PREFIX = INSTAGRAM_API_PREFIX;
var INSTAGRAM_FETCH_MEDIA_URL = "https://graph.instagram.com/me/media";
exports.INSTAGRAM_FETCH_MEDIA_URL = INSTAGRAM_FETCH_MEDIA_URL;
var INSTAGRAM_FETCH_USER_URL = "https://graph.instagram.com/me";
exports.INSTAGRAM_FETCH_USER_URL = INSTAGRAM_FETCH_USER_URL;
var INSTAGRAM_REFRESH_TOKEN = "https://graph.instagram.com/refresh_access_token";
exports.INSTAGRAM_REFRESH_TOKEN = INSTAGRAM_REFRESH_TOKEN;